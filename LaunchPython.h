//---------------------------------------------------------------------------
#ifndef LaunchPythonH
#define LaunchPythonH
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>

#include <boost/config.hpp>
//---------------------------------------------------------------------------

class LaunchPython {
   protected:
	  //const std::string projectDIR;
	  const std::string pythonFolder;
	  std::string pythonProgram;
	  LaunchPython();

   public:
	  int execute();
	  int push_data(std::vector<float> &data);
	  void updateINI(std::string line, float value);
};

class LaunchPyspect: public LaunchPython {
   public:
	  LaunchPyspect();
};
#endif
