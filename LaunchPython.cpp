//---------------------------------------------------------------------------
#pragma hdrstop
#include "LaunchPython.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>

#include <boost/program_options.hpp>
#include <boost/program_options/detail/config_file.hpp>
#include <boost/program_options/parsers.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

using namespace std;

LaunchPython::LaunchPython():pythonFolder("C:/Users/John/OneDrive - Novaminex/Documents/Embarcadero/Studio/Projects/LaunchPythonDemo/pySpect/"),         //some default value pointing to a known program folder
pythonProgram("main.exe"){     //Some default program
}

int LaunchPython::execute() {
	///RUN PROGRAM
	//cout << "Running program "+pythonProgram+":" <<endl;
	cout << "\""+pythonFolder+pythonProgram+"\"" <<endl;
	system(("\""+pythonFolder+pythonProgram+"\"").c_str());
	return 0;
}

int LaunchPython::push_data(std::vector<float> &data) {
	//Save binary data to folder containing python script:
	string name = "windowTimeSeries.dat";
	cout << "Writing " << pythonFolder+name << '\n';
	std::ofstream myFile((pythonFolder+name).c_str(), ios::out | ios::binary);
	int size= (sizeof(float))*data.size();
	myFile.write (reinterpret_cast<char*>(&data[0]), size);
	return size;
}

//INI FILE FORMAT:
//[data]
//samplingRate=50000
//startSeconds=500
//endSeconds=2700
//[spectrogram]
//decimationFactor1 = 20
//decimationFactor2 = 5
//spectrogramXScale = 100
//Log10MinSpectrum = -12
//Log10MaxSpectrum = -4
//nfft = 1024
//plotHeight = 12.0

void LaunchPython::updateINI(string line, float value){
	///READ INI FILE SCANNING FOR "line" AND UPDATE WITH "value"
	using namespace boost::property_tree;
	boost::property_tree::ptree pt;
	int L=pythonProgram.length();
	string programINI= pythonProgram.substr(0,(L-3))+"ini";
	//programINI=projectDIR+programINI;
	boost::property_tree::ini_parser::read_ini(programINI, pt);
	//Search for line among parameter labels for INI file:
	vector<string> labels;
	//Read them from INI file:
	for (const std::pair<std::string, ptree> &p1 : pt){
		//std::cout << p1.first << '\n';
		string temp =   p1.first;
		ptree &c = pt.get_child(temp);
			for (const std::pair<std::string, ptree> &p1 : c){
				labels.push_back(temp+"."+p1.first);
			}
	}
	//Find matching line:
	if( std::find(labels.begin(),labels.end(),line) != labels.end()){
		pt.put(line,value);      //Replacement value
	}
	else{
		throw std::runtime_error("Invalid entry for INI file: "+line);
	}
	//Rewrite INI file:
	ofstream myfile (programINI);
	if (myfile.is_open())
	{
	//Write them back out:
		for (const std::pair<std::string, ptree> &p1 : pt){
		myfile << "["<<p1.first<<"]" << '\n';
		string temp =   p1.first;
		ptree &c = pt.get_child(temp);
			for (const std::pair<std::string, ptree> &p1 : c){
				myfile << p1.first <<" = "<< p1.second.get_value<string>() << '\n';
			}
		}
	}
	else{
		throw std::runtime_error("Unable to open INI file: "+programINI);
	}
}

LaunchPyspect::LaunchPyspect(){
	///Hard code the relative locations for the python script and INI file:
	//pythonFolder = "C:/Users/John/OneDrive - Novaminex/Documents/Embarcadero/Studio/Projects/LaunchPythonDemo/pySpect/";//
	//pythonFolder = "../../pySpect/";   //Relative folder
	pythonProgram = "pySpect.exe";
};
