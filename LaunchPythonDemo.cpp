#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <fstream>
#include <iomanip>

#include "LaunchPython.h"

int _tmain(int argc, _TCHAR* argv[])
{
	//----------------------------------------------
	///Load sample time series which in EMS would already be in memory ready for display:
    using namespace std;
	streampos begin,end;
	string name = "../../SampleTimeSeries.dat"; //Home directory of project (EMS)
	ifstream myfile (name, ios::binary);
	if (!(myfile.is_open())){
		throw runtime_error(name+" is missing or will not open");
	}
	begin = myfile.tellg();
	myfile.seekg (0, ios::end);
	end = myfile.tellg();
	//cout << "size is: " << (end-begin) << " bytes.\n";
	myfile.seekg (0, ios::beg);
	//
	if((static_cast<uint64_t>(end-begin)%sizeof(float))!=0){
		throw runtime_error(name+" is incorrect length: "+to_string(end-begin)+" bytes");
	}
	uint64_t N=static_cast<uint64_t>(end-begin)/sizeof(float);
	if (N==0){
		throw runtime_error(name+" is empty");
	}
	vector<float> vec(N);// reserve space for N entries of type float
	myfile.read(reinterpret_cast<char*>(vec.data()), (end-begin));
	myfile.close();
	//----------------------------------------------
	//Example of using class for handling of data and python script:
	LaunchPyspect programLaunch;
	LaunchPython * pythonLaunch = &programLaunch;
	///Example of switching a data label in ini file:
	pythonLaunch->updateINI("data.samplingRate",50000);
	//Transfering data to location of python script:
	pythonLaunch->push_data(vec);
	//Display result:
	pythonLaunch->execute();
	return 0;
}

